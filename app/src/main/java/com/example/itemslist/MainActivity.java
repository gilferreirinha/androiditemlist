package com.example.itemslist;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import com.example.itemslist.ItemPackage.ItemText;
import com.example.itemslist.ItemPackage.ItemTextAdapter;

import org.xml.sax.Parser;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    // properties
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    // define list
    private ArrayList<ItemText> itemArrayList = new ArrayList<ItemText>();

    // create item struct
    private ItemText itemText =
            new ItemText(0,null,  "Levar livro",
            "livro de android e de python");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LunchNewActivity();
            }
        });

        // add item to array
        itemText = new ItemText(0,null,  "Criar post", "Desenvolver o design para o site");
        itemArrayList.add(itemText);
        itemText = new ItemText(0,null,  "Gravar Tack", "Com andamento 120");
        itemArrayList.add(itemText);
        itemText = new ItemText(0,null,  "Acabar Api", "Não esquecer que tem que ser Rest e Soap");
        itemArrayList.add(itemText);
        itemText = new ItemText(0,null,  "Dentista", "Levar exame");
        itemArrayList.add(itemText);

        // recognize recyclerView
        recyclerView = (RecyclerView)findViewById(R.id.mainList);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ItemTextAdapter(itemArrayList);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_clear_all) {

            itemArrayList.clear();
            recyclerView.setAdapter(mAdapter);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    static final int CREATE_ITEM_REQUEST = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Check which request we're responding to
        if (requestCode == CREATE_ITEM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == CreateItem.RESULT_OK) {

                String subject, content;
                int hour, minute, seconds, day, month, year;
                final Calendar myCalendar = Calendar.getInstance();

                subject = getIntent().getStringExtra("subject");
                content = getIntent().getStringExtra("content");

                // get data and define this
                hour = getIntent().getIntExtra("year", 0);
                minute = getIntent().getIntExtra("month", 0);
                seconds = getIntent().getIntExtra("seconds", 0);
                day = getIntent().getIntExtra("day", 0);
                month = getIntent().getIntExtra("hour", 0);
                year = getIntent().getIntExtra("minute", 0);

                myCalendar.set(year, month, day, hour, minute, seconds);

                ItemText newItem = new ItemText(0,
                        myCalendar,
                        subject,
                        content
                );

                // add adapter to array
                itemArrayList.add(newItem);

                // set adapter to recyclerView
                recyclerView.setAdapter(mAdapter);
            }
        }
    }

    // open create item activity
    public void LunchNewActivity()
    {
        Intent intent = new Intent(this, CreateItem.class);
        startActivityForResult(intent, CREATE_ITEM_REQUEST);
    }
}
