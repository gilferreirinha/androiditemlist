package com.example.itemslist.ItemPackage;

import com.example.itemslist.ItemPackage.Item;

import java.util.Calendar;
import java.util.Date;

/*
    a child class of Item
    this represent the ItemText
*/
public class ItemText extends Item {

    // properties
    private String subject;
    private String content;

    // constructor
    public ItemText() {
        super(0, null);
        this.subject = null;
        this.content = null;
    }
    public ItemText(int id, Calendar dateTime, String subject, String content) {
        super(id, dateTime);
        this.subject = subject;
        this.content = content;
    }

    // getters and setters
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
