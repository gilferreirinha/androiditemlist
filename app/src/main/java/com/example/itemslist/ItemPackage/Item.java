package com.example.itemslist.ItemPackage;

import java.util.Calendar;
import java.util.Date;

// father Item class
public class Item {

    // properties
    private int id;
    private Calendar dateTime;

    // constructor
    public Item() {
        this.id = 0;
        this.dateTime = null;
    }

    public Item(int id, Calendar dateTime) {
        this.id = id;
        this.dateTime = dateTime;
    }

    // getters and setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Calendar getDateTime() {
        return dateTime;
    }

    // personalize getDate
    public String  getDate() {
        String date = null;

        if (dateTime != null)
        {
            date =  dateTime.get(Calendar.YEAR) + " - " +
                    dateTime.get(Calendar.MONTH) + " - " +
                    dateTime.get(Calendar.DAY_OF_MONTH);
        }

        return date;
    }

    // personalize getTime
    public String  getTime() {
        String time = "";

        if (dateTime != null)
        {
            time =  dateTime.get(Calendar.HOUR) + ":" +
                    dateTime.get(Calendar.MINUTE) + ":" +
                    dateTime.get(Calendar.SECOND);
        }
        return  time;
    }
}




