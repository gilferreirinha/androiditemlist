package com.example.itemslist.ItemPackage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.itemslist.R;
import java.util.ArrayList;

public class ItemTextAdapter extends RecyclerView.Adapter<ItemTextAdapter.MyViewHolder> {
    private ArrayList<ItemText> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView subject, cotent, date, time ;

        public MyViewHolder(View view) {
            super(view);

            subject = (TextView) view.findViewById(R.id.textViewSubject);
            cotent = (TextView) view.findViewById(R.id.textViewContent);
            date = (TextView) view.findViewById(R.id.txt_data);
            time = (TextView) view.findViewById(R.id.txt_time);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemTextAdapter(ArrayList<ItemText> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ItemTextAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);

        return  new MyViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        // get item
        ItemText itemText = mDataset.get(position);

        // define text
        holder.subject.setText(itemText.getSubject());
        holder.cotent.setText(itemText.getContent());
        holder.date.setText(itemText.getDate());
        holder.time.setText(itemText.getTime());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
