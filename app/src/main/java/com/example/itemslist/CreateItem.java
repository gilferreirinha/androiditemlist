package com.example.itemslist;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import java.util.Calendar;

public class CreateItem extends AppCompatActivity {

    EditText inputSubject, inputContent;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog datePicker;
    TimePickerDialog timePicker;
    Button btnSave;
    ImageButton btnTime, btnDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        inputSubject = (EditText) findViewById(R.id.input_subject);
        inputContent = (EditText) findViewById(R.id.input_content);
        btnTime = (ImageButton) findViewById(R.id.input_time);
        btnDate = (ImageButton) findViewById(R.id.input_date);
        btnSave= (Button) findViewById(R.id.btn_save);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 new DatePickerDialog(CreateItem.this,
                         (DatePickerDialog.OnDateSetListener) datePicker,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(CreateItem.this,
                        (TimePickerDialog.OnTimeSetListener) timePicker,
                        myCalendar.get(Calendar.HOUR),
                        myCalendar.get(Calendar.MINUTE),
                        true).show();
            }
        });

       btnSave.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               Intent intent = new Intent(CreateItem.this, MainActivity.class);

               // string subject and content
               intent.putExtra("subject", inputSubject.getText().toString());
               intent.putExtra("content", inputContent.getText().toString());

               // date and hour
               intent.putExtra("year", myCalendar.get(Calendar.YEAR));
               intent.putExtra("month", myCalendar.get(Calendar.MONTH));
               intent.putExtra("day", myCalendar.get(Calendar.DAY_OF_MONTH));
               intent.putExtra("hour",  myCalendar.get(Calendar.MINUTE));
               intent.putExtra("minute", myCalendar.get(Calendar.MINUTE));

               // set result
               setResult(CreateItem.RESULT_OK, intent);

               // finish the activity
               finish();
           }
       });
    }
}
